---
layout: default
home:
  background-image: /assets/img/header_bg.jpg
  panorama-image: /assets/img/footer_panorama.jpg
  site-subtitle: Studio d'enregistrement et de mixage à Lyon – Fr
  site-title: Sample & Hold
studio:
  description: >
    Le studio est composé d'une live room  à l'acoustique claire et
    chaleureuse,  ainsi que d'une régie à l'écoute précise
  diapos:
    - url: /assets/img/diapo_pieces_1.jpg
    - url: /assets/img/diapo_pieces_2.jpg
    - url: /assets/img/diapo_pieces_3.jpg
    - url: /assets/img/diapo_pieces_4.jpg
    - url: /assets/img/diapo_pieces_5.jpg
  title: Le studio
materiel:
  description: >
    Le studio est centré autour d'une console analogique vintage SAJE qui
    combine caractère et polyvalence. De nombreux micros, compresseurs et autres
    périphériques datant des années 50 à nos jours sont là pour proposer toutes
    les couleurs nécessaires.
  diapos:
    - url: /assets/img/diapo_matos_1.jpg
    - url: /assets/img/diapo_matos_2.jpg
    - url: /assets/img/diapo_matos_3.jpg
    - url: /assets/img/diapo_matos_4.jpg
    - url: /assets/img/diapo_matos_5.jpg
  title: Le matériel
humains:
  description: >
    Théo DAS NEVES est <b>musicien, producteur et ingénieur du son</b>. Sa
    carrière le spécialise dans les <b>musiques électroniques, pop et indie</b>.
    Diplômé en électronique, il gère, conçoit et maintient le studio et ses
    équipements.
  image: /assets/img/dehu.jpg
  title: L'humain
references:
  description: |
    Un aperçu des collaborations passées
  ref-list:
    - name: Terrenoire
      image: /assets/img/terrenoire-forces-contraires.jpg
      realisation: Co-réalisation
      social:
        - title: facebook
          url: https://www.facebook.com/TerrenoireLaVieLaMort
        - title: instagram
          url: https://www.instagram.com/_terrenoire_/
    - image: assets/img/ref_1.jpg
      name: Luje
      realisation: Réalisation - Enregistrement - Mixage
      social:
        - title: facebook
          url: https://www.facebook.com/lujeband/
        - title: instagram
          url: https://www.instagram.com/luje_band/
    - image: /assets/img/wugo.jpg
      name: Wugo
      realisation: Co-Réalisation - Mixage
      social:
        - title: facebook
          url: https://www.facebook.com/iamwugo
        - title: instagram
          url: https://www.instagram.com/iamwugo/
    - image: /assets/img/wwwwwww.jpg
      name: Wild Wild Waves
      realisation: Enregistrement
      social:
        - title: facebook
          url: https://www.facebook.com/wildwildwaves/
        - title: instagram
          url: https://www.instagram.com/wildwildwavesmusic/
        - title: link
          url: http://wildwildwaves.fr
    - image: assets/img/ref_2.jpg
      name: please_don't
      realisation: Réalisation - Enregistrement - Mixage
      social:
        - title: instagram
          url: https://www.instagram.com/please_dont_make_music/
        - title: link
          url: https://www.pleasedont.fr
    - image: /assets/img/Hayko.png
      name: Hayko
      realisation: Réalisation - Enregistrement - Mixage
      social:
        - title: facebook
          url: https://www.facebook.com/ImHayko
        - title: instagram
          url: https://www.instagram.com/i_hayko/
    - image: /assets/img/hangoyster.jpg
      name: Hangoyster
      realisation: Co-Réalisation - Enregistrement - Mixage
      social:
        - title: facebook
          url: https://www.facebook.com/hangoyster
        - title: instagram
          url: https://www.instagram.com/hangoyster/
  title: Les Références du studio
contact:
  description: N'hésitez pas à nous contacter pour discuter de votre projet !
  title: Contact
  subtitle: " "
---
